FROM python:3.11.6-bullseye
# Or any preferred Python version.
RUN mkdir /app
WORKDIR /app
ADD app /app
RUN apt-get update
RUN apt-get install -y libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 build-essential software-properties-common
RUN pip install -r requirements.txt
# CMD ["python", "-m", "streamlit", "run", "/app/main.py"]
EXPOSE 8501
ENTRYPOINT ["streamlit", "run", "main.py", "--server.port=8501", "--server.address=0.0.0.0"]