from app_functions.os_ops import open_calculator, open_camera, open_cmd, open_notepad, open_discord
from app_functions.online_ops import find_my_ip, get_latest_news, get_random_advice, get_random_joke, get_trending_movies, get_weather_report, play_on_youtube, search_on_google, search_on_wikipedia
from app_functions.repos import create_repo
import speech_recognition as sr
from flask import Flask, render_template, request
import pyttsx3
import openai
import jsonify
import os
from pprint import pprint
import requests
import autogen
from autogen import AssistantAgent
from dotenv import load_dotenv
load_dotenv()

#config_list = autogen.config_list_from_json("OAI_CONFIG_LIST")
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
OPENAI_API_BASE = os.getenv('OPENAI_API_BASE')
## api keys for the memGPT
openai.api_base = OPENAI_API_BASE
openai.api_key= OPENAI_API_KEY
import openai
app = Flask(__name__)
# Function to convert
# text to speech
def speak(command):

    try:
        # Initialise the engine
        engine = pyttsx3.init()
        # Change speech rate
        engine.setProperty('rate', 180)

        # Get available voices
        voices = engine.getProperty('voices')

        # choose a voice based on voice ID
        engine.setProperty('voice', voices[1].id)
        engine.say(command)
        engine.runAndWait()
        print(command)
    except:
        print("Error: Could not speak the text.")

# Initialise the recogniser
r = sr.Recognizer()

# Define a function to recognize speech
def recognize_speech():
    try:
        r = sr.Recognizer()
        with sr.Microphone() as source:
            audio = r.listen(source)
            query = r.recognize_google(audio)
            return query
    except KeyboardInterrupt:
        print("Keyboard interrupt detected. Closing the program...")
    except:
        print("Error: Could not recognize speech.")
        return ""

# Define a function to handle non-audible sounds
def handle_non_audible_sounds():
    try:
        r = sr.Recognizer()
        with sr.Microphone() as source:
            audio = r.listen(source)
        pass
        return True
    except:
        print("Error: Could not handle non-audible sounds.")
        pass
        return False
    

def send_to_chatGPT(messages, model="gpt-3.5-turbo"):
    # Send query to ChatGPT
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        max_tokens=100,
        n=1,
        stop=None,
        temperature=0.2,
    )

    message = response.choices[0].message.content
    messages.append(response.choices[0].message)
    return message

messages = [{"role": "user", "content": "Please, Act like Jarvis AI from Iron Man, without saying something to introduce yourself that sounds like this chat has already started. Be funny and a little sarcastic"}]

config_list =     [
    {
        "api_type" : "open_ai",
        "api_base": f"{OPENAI_API_BASE}",
        "api_key": f"{OPENAI_API_KEY}"
    },
    {
        "model": "gpt-4",
        "api_key": "<your Azure OpenAI API key here>",
        "api_base": "<your Azure OpenAI API base here>",
        "api_type": "azure",
        "api_version": "2023-06-01-preview"
    },
    {
        "model": "gpt-4-32k",
        "api_key": "<your Azure OpenAI API key here>",
        "api_base": "<your Azure OpenAI API base here>",
        "api_type": "azure",
        "api_version": "2023-06-01-preview"
    }
]

# create an AssistantAgent named "assistant"
assistant = AssistantAgent(
    name="assistant",
    llm_config={
        "seed": 42,  # seed for caching and reproducibility
        "config_list": config_list,  # a list of OpenAI API configurations
        "temperature": 0,  # temperature for sampling
    },  # configuration for autogen's enhanced inference API which is compatible with OpenAI API
)
# create a UserProxyAgent instance named "user_proxy"
user_proxy = autogen.UserProxyAgent(
    name="user_proxy",
    human_input_mode="NEVER",
    max_consecutive_auto_reply=10,
    is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
    code_execution_config={
        "work_dir": "../coding_output",
        "use_docker": False,  # set to True or image name like "python:3" to use docker
    },
)

@app.route('/')
def index():
# Wait for the wake word
    print("Listening...")
    #if st.button('Record'):
    query = recognize_speech()
    #query = recognize_speech()
    if "exit" in query.lower() or "stop" in query.lower():
        speak("Goodbye!")
        exit()

    if "jarvis" in query.lower():
        speak("How can I help you?")
        #print("How can I help you?")
        while True:
            # Wait for the user's command
            query = recognize_speech()
            if "exit" in query.lower() or "stop" in query.lower():
                speak("Goodbye!")
                break
            else:
                # Handle the user's command
                if "open notepad" in query.lower():
                    speak("Opening Notepad...")
                    open_notepad()
                    break

                elif "open calculator" in query.lower():
                    speak("Opening Calculator...")
                    open_calculator()
                    break

                elif "open camera" in query.lower():
                    speak("Opening Camera...")
                    open_camera()
                    break

                elif "open discord" in query.lower():
                    speak("Opening Discord...")
                    open_discord()
                    break

                elif 'open command prompt' in query.lower() or 'open cmd' in query.lower():
                    speak("Opening Command Prompt...")
                    open_cmd()
                    break

                elif 'ip address' in query.lower():
                    ip_address = find_my_ip()
                    speak(f'Your IP Address is {ip_address}.\n For your convenience, I am printing it on the screen sir.')
                    print(f'Your IP Address is {ip_address}')
                    break

                elif 'wikipedia' in query.lower():
                    speak('What do you want to search on Wikipedia, sir?')
                    search_query = recognize_speech()
                    results = search_on_wikipedia(search_query)
                    speak(f"According to Wikipedia, {results}")
                    speak("For your convenience, I am printing it on the screen sir.")
                    print(results)
                    break

                elif 'youtube' in query.lower():
                    speak('What do you want to play on Youtube Sir?')
                    video = recognize_speech()
                    play_on_youtube(video)
                    break

                elif 'search on google' in query.lower():
                    speak('What do you want to search on Google Sir?')
                    query = recognize_speech()
                    search_on_google(query)
                    break

                elif 'create repository' in query.lower():
                    speak('What do you want the repository to be called Sir?')
                    repo_name = recognize_speech()
                    create_repo(repo_name)
                    speak('Your repository has been created for you Sir.')
                    break

                elif 'joke' in query.lower():
                    speak(f"Hope you like this one Sir")
                    joke = get_random_joke()
                    speak(joke)
                    speak("For your convenience, I am also printing it on the screen Sir.")
                    pprint(joke)
                    break

                elif "advice" in query.lower():
                    speak(f"Here's some advice for you, sir")
                    advice = get_random_advice()
                    speak(advice)
                    speak("For your convenience, I am also printing it on the screen Sir.")
                    pprint(advice)
                    break

                # elif "question" in query.lower():
                #     speak(f"Ask away Sir!")
                #     question = recognize_speech()
                #     result = retrieval.agent_executor({"input": question})
                #     speak(result["output"])
                #     speak("For your convenience, I am printing it on the screen Sir.")
                #     pprint(result["output"])
                #     break

                elif "trending movies" in query.lower():
                    speak(f"Some of the trending movies are: {get_trending_movies()}")
                    speak("For your convenience, I am printing it on the screen Sir.")
                    print(*get_trending_movies(), sep='\n')
                    break

                elif 'news' in query.lower():
                    speak(f"I'm reading out the latest news headlines Sir")
                    speak(get_latest_news())
                    speak("For your convenience, I am printing it on the screen Sir.")
                    print(*get_latest_news(), sep='\n')
                    break

                elif 'weather' in query.lower():
                    ip_address = find_my_ip()
                    city = requests.get(f"https://ipapi.co/{ip_address}/city/").text
                    speak(f"Getting weather report for your city {city}")
                    weather, temperature, feels_like = get_weather_report(city)
                    speak(f"The current temperature is {temperature}, but it feels like {feels_like}")
                    speak(f"Also, the weather report talks about {weather}")
                    speak("For your convenience, I am printing it on the screen Sir.")
                    print(f"Description: {weather}\nTemperature: {temperature}\nFeels like: {feels_like}")
                    break

                elif 'python app' in query.lower():
                    speak('What do you want to create in Python Sir?')
                    query = recognize_speech()
                    speak('Requesting an agent create this for you.')
                    speak('Please see the terminal for code output.')
                    # the assistant receives a message from the user_proxy, which contains the task description
                    user_proxy.initiate_chat(
                        assistant,
                        message=query,
                    )
                    is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE")
                    if is_termination_msg:
                        speak('Your Program has been created for you Sir.')
                        speak('Please check the coding output folder for your code')
                        break
                    else:
                        speak('An agent is processing your request, Sir')
                        break

                else:
                    # speak("Handing over to Open AI for help.")
                    # speak("What do you need from Open AI, Sir?")
                    #query = recognize_speech()
                    messages.append({"role": "user", "content": query})
                    response = send_to_chatGPT(messages)
                    #print(f"{response}")
                    speak(response)
                    break
    return render_template('index.html')

@app.route('/transcribe', methods=['POST'])
def transcribe_audio():
    try:

        transcription = recognize_speech()

        return ({"message": transcription})

    except Exception as e:
        return jsonify({"error": str(e)})

# # Main loop
# def main():
#     try:
#         while True:
#             # Wait for the wake word
#             print("Listening...")
#             #if st.button('Record'):
#             query = recognize_speech()
#             #query = recognize_speech()
#             if "exit" in query.lower() or "stop" in query.lower():
#                 speak("Goodbye!")
#                 exit()

#             if "jarvis" in query.lower():
#                 speak("How can I help you?")
#                 #print("How can I help you?")
#                 while True:
#                     # Wait for the user's command
#                     query = recognize_speech()
#                     if "exit" in query.lower() or "stop" in query.lower():
#                         speak("Goodbye!")
#                         break
#                     else:
#                         # Handle the user's command
#                         if "open notepad" in query.lower():
#                             speak("Opening Notepad...")
#                             open_notepad()
#                             break

#                         elif "open calculator" in query.lower():
#                             speak("Opening Calculator...")
#                             open_calculator()
#                             break

#                         elif "open camera" in query.lower():
#                             speak("Opening Camera...")
#                             open_camera()
#                             break

#                         elif "open discord" in query.lower():
#                             speak("Opening Discord...")
#                             open_discord()
#                             break

#                         elif 'open command prompt' in query.lower() or 'open cmd' in query.lower():
#                             speak("Opening Command Prompt...")
#                             open_cmd()
#                             break

#                         elif 'ip address' in query.lower():
#                             ip_address = find_my_ip()
#                             speak(f'Your IP Address is {ip_address}.\n For your convenience, I am printing it on the screen sir.')
#                             print(f'Your IP Address is {ip_address}')
#                             break

#                         elif 'wikipedia' in query.lower():
#                             speak('What do you want to search on Wikipedia, sir?')
#                             search_query = recognize_speech()
#                             results = search_on_wikipedia(search_query)
#                             speak(f"According to Wikipedia, {results}")
#                             speak("For your convenience, I am printing it on the screen sir.")
#                             print(results)
#                             break

#                         elif 'youtube' in query.lower():
#                             speak('What do you want to play on Youtube Sir?')
#                             video = recognize_speech()
#                             play_on_youtube(video)
#                             break

#                         elif 'search on google' in query.lower():
#                             speak('What do you want to search on Google Sir?')
#                             query = recognize_speech()
#                             search_on_google(query)
#                             break

#                         elif 'joke' in query.lower():
#                             speak(f"Hope you like this one Sir")
#                             joke = get_random_joke()
#                             speak(joke)
#                             speak("For your convenience, I am also printing it on the screen Sir.")
#                             pprint(joke)
#                             break

#                         elif "advice" in query.lower():
#                             speak(f"Here's some advice for you, sir")
#                             advice = get_random_advice()
#                             speak(advice)
#                             speak("For your convenience, I am also printing it on the screen Sir.")
#                             pprint(advice)
#                             break

#                         # elif "question" in query.lower():
#                         #     speak(f"Ask away Sir!")
#                         #     question = recognize_speech()
#                         #     result = retrieval.agent_executor({"input": question})
#                         #     speak(result["output"])
#                         #     speak("For your convenience, I am printing it on the screen Sir.")
#                         #     pprint(result["output"])
#                         #     break

#                         elif "trending movies" in query.lower():
#                             speak(f"Some of the trending movies are: {get_trending_movies()}")
#                             speak("For your convenience, I am printing it on the screen Sir.")
#                             print(*get_trending_movies(), sep='\n')
#                             break

#                         elif 'news' in query.lower():
#                             speak(f"I'm reading out the latest news headlines Sir")
#                             speak(get_latest_news())
#                             speak("For your convenience, I am printing it on the screen Sir.")
#                             print(*get_latest_news(), sep='\n')
#                             break

#                         elif 'weather' in query.lower():
#                             ip_address = find_my_ip()
#                             city = requests.get(f"https://ipapi.co/{ip_address}/city/").text
#                             speak(f"Getting weather report for your city {city}")
#                             weather, temperature, feels_like = get_weather_report(city)
#                             speak(f"The current temperature is {temperature}, but it feels like {feels_like}")
#                             speak(f"Also, the weather report talks about {weather}")
#                             speak("For your convenience, I am printing it on the screen Sir.")
#                             print(f"Description: {weather}\nTemperature: {temperature}\nFeels like: {feels_like}")
#                             break

#                         elif 'python app' in query.lower():
#                             speak('What do you want to create in Python Sir?')
#                             query = recognize_speech()
#                             speak('Requesting an agent create this for you.')
#                             speak('Please see the terminal for code output.')
#                             # the assistant receives a message from the user_proxy, which contains the task description
#                             user_proxy.initiate_chat(
#                                 assistant,
#                                 message=query,
#                             )
#                             is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE")
#                             if is_termination_msg:
#                                 speak('Your Program has been created for you Sir.')
#                                 speak('Please check the coding output folder for your code')
#                                 break
#                             else:
#                                 speak('An agent is processing your request, Sir')
#                                 break

#                         else:
#                             # speak("Handing over to Open AI for help.")
#                             # speak("What do you need from Open AI, Sir?")
#                             #query = recognize_speech()
#                             messages.append({"role": "user", "content": query})
#                             response = send_to_chatGPT(messages)
#                             #print(f"{response}")
#                             speak(response)
#                             break

#             else:
#                 #speak("Sorry, I don't understand. Please try again.")
#                 handle_non_audible_sounds()
#     except KeyboardInterrupt:
#         print("Keyboard interrupt detected. Closing the program...")

if __name__ == "__main__":
    app.run(debug=True)
