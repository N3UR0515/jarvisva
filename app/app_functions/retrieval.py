import os
import sys
from dotenv import load_dotenv
load_dotenv()

from langchain.document_loaders import TextLoader
loader = TextLoader('./data/data.txt')
#from langchain.document_loaders import DirectoryLoader
#loader = DirectoryLoader("./data", glob="*.txt")
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import FAISS
from langchain.embeddings import OpenAIEmbeddings

from langchain.agents.agent_toolkits import create_retriever_tool
from langchain.agents.agent_toolkits import create_conversational_retrieval_agent

OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
OPENAI_API_BASE = os.getenv('OPENAI_API_BASE')

documents = loader.load()
text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=0)
texts = text_splitter.split_documents(documents)
embeddings = OpenAIEmbeddings()
db = FAISS.from_documents(texts, embeddings)

retriever = db.as_retriever()

tool = create_retriever_tool(
    retriever, 
    "search_data",
    "Searches and returns information regarding the user."
)
tools = [tool]

from langchain.chat_models import ChatOpenAI
llm = ChatOpenAI(temperature = 0)
agent_executor = create_conversational_retrieval_agent(llm, tools, verbose=True)

#result = agent_executor({"input": "What is my schedule?"})