import gitlab
import os

repo_name = ''

def create_repo(repo_name):
# create a GitLab instance
    gl = gitlab.Gitlab('https://gitlab.com', private_token='glpat-MgwrCjifbMzzbcyMyB2T')

    # Create the repo directory
    os.mkdir(f'C:/Users/Alfie/REPOS/{repo_name}')

    # Test Gitlab authentication
    gl.auth()

    # Create repo on GitLab using the GitLab API
    project = gl.projects.create({'name': repo_name, 'description': f'This is my project for {repo_name}', 'visibility': 'private'})
    print("Repo created successfully!")

    # create a new file in a branch
    file_path = './README.md'
    content = f'{repo_name} created!'
    commit_message = 'Initial commit'
    branch_name = 'main'

    file = project.files.create({'file_path': file_path, 'branch': branch_name, 'content': content, 'commit_message': commit_message})
    print("File created successfully!")