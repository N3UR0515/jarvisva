@echo off
setlocal enabledelayedexpansion

if exist .env (
    for /f "tokens=1,* delims==" %%A in (.env) do (
        set %%A=%%B
    )
)

if not defined OPENAI_API_BASE (
    echo Environment variable OPENAI_API_BASE is not defined.
    pause
    exit /b 1
)

if not defined OPENAI_API_KEY (
    echo Environment variable OPENAI_API_KEY is not defined.
    pause
    exit /b 1
)

set OPENAI_API_BASE=%OPENAI_API_BASE%
set OPENAI_API_KEY=%OPENAI_API_KEY%

cd app/

python main.py

pause
